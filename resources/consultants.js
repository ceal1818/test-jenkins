let ee = require("../services");
let route = require("express").Router();

const PATH = "/consultants";

route.get(PATH+"/:id", (req, res) => {
    let id = req.params.id;
    ee.on("consultant:get:success", function(entity){
        res.status(200)
            .json(entity);
    });
    
    ee.on("consultant:get:unsuccess", function(err){
        res.status(400)
            .json({
                message: "Not found..."
            });
    });

    ee.emit("consultant:get", id);
});

route.get(PATH, (req, res) => {
    ee.on("consultant:list:success", function(rows){
        res.status(200)
            .json(rows);
    });
    
    ee.on("consultant:list:unsuccess", function(err){
        res.status(400)
            .json({
                message: "Error..."
            });
    });

    ee.emit("consultant:list");
});

route.post(PATH, (req, res) => {
    let consultant = req.body;
    ee.on("consultant:create:success", function(entity){
        res.status(201)
            .json(entity);
    });
    
    ee.on("consultant:create:unsuccess", function(err){
        res.status(400)
            .json({
                message: "Error..."
            });
    });

    ee.emit("consultant:create", consultant);
});

route.put(PATH, (req, res) => {
    let consultant = req.body;
    let id = consultant.id;

    ee.on("consultant:get:success", function(entity){
        console.log(entity);
        ee.on("consultant:set:success", function(_entity){
            res.status(200)
                .json(_entity);
        });
        
        ee.on("consultant:set:unsuccess", function(err){
            res.status(400)
                .json({
                    message: "Error..."
                });
        });
    
        ee.emit("consultant:set", consultant);
    });
    
    ee.on("consultant:get:unsuccess", function(err){
        res.status(404)
            .json({
                message: "Not found..."
            });
    });

    ee.emit("consultant:get", id);
});

route.delete(PATH+"/:id", (req, res) => {
    let id = req.params.id;

    ee.on("consultant:get:success", function(_entity){
        ee.on("consultant:remove:success", function(){
            res.status(200).send();
        });
        
        ee.on("consultant:remove:unsuccess", function(err){
            res.status(400)
                .json({
                    message: "Error..."
                });
        });
    
        ee.emit("consultant:remove", _entity);
    });
    
    ee.on("consultant:get:unsuccess", function(err){
        res.status(404)
            .json({
                message: "Not found..."
            });
    });

    ee.emit("consultant:get", id);
});

module.exports = route;