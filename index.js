let express = require("express");
let bodyParser = require("body-parser");
let consultants = require("./resources/consultants");
let app = express();
const WEB_PORT = process.env.PORT || 9090;

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use("/api", consultants);

module.export = app.listen(WEB_PORT, function(err){
    if (err){
        console.log(err);
    } else {
        console.log("Ready by ", WEB_PORT);
    }
});
