let repository = require("../../repositories").consultantRepository;
let _ = require("underscore");

module.exports = class Add{

    constructor(ee){
        this._ee = ee;
        this._ee.on("consultant:create", (entity) => {
            repository.add(entity)
                .then((_entity) => {
                    this._ee.emit("consultant:create:success", _.extend(entity, _entity));
                }, (err)  => {
                    this._ee.emit("consultant:create:unsuccess", err);
                });        
        });
    }

}