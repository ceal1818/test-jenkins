let repository = require("../../repositories").consultantRepository;
let _ = require("underscore");

module.exports = class Update{

    constructor(ee){
        this._ee = ee;
        this._ee.on("consultant:set", (entity) => {
            repository.set(entity)
                .then((_entity) => {
                    this._ee.emit("consultant:set:success",  _.extend(entity, _entity));
                }, (err)  => {
                    this._ee.emit("consultant:set:unsuccess", err);
                });        
        });
    }
    
}